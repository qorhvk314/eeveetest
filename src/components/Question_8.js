import React  from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { increaseValues } from './actions';
import { useSelector } from 'react-redux';

const QuestionTitle = styled.div`

    text-align: center;
    position: relative;
    left: 50%;
    transform: translateX(-50%);

    .btnQ {
        top: 50%;
        left: 50%;
        width: 250px;
        margin-top: -86px;
        margin-left: -89px;
        text-align: center;
        -webkit-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        -moz-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        -ms-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        -o-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        display: block;
        margin: 20px auto;
        text-decoration: none;
        border-radius: 10px;
        padding: 10px 20px;
        color: rgba(30, 22, 54, 0.6);
	    box-shadow: rgba(30, 22, 54, 0.4) 0 0px 0px 2px inset;
    }

        .btnQ:hover {
        color: rgba(255, 255, 255, 0.85);
        box-shadow: rgba(30, 22, 54, 0.7) 0 0px 0px 40px inset;
    }

    h1{
        text-align: center;
        font-size: 20px;
        margin-top: 40px;
        margin-bottom: 30px;
    }

`;

function Question_2 () {

    const dispatch = useDispatch();
    const values = useSelector(state => state.values);

    const plus1 = () => {
        dispatch(increaseValues({umbreon: values.umbreon + 1, leafeon: values.leafeon + 1, flareon: values.flareon + 1}));
    }
    
    const plus2 = () => {
        dispatch(increaseValues({espeon: values.sylveon + 1, glaceon: values.glaceon + 1, jolteon: values.jolteon + 1}));
    }
    
    const plus3 = () => {
        dispatch(increaseValues({vaporeon: values.vaporeon + 1, sylveon: values.espeon + 1}));
    }

    console.log(values)

    return (
        <QuestionTitle>
            <div className="Img1">
                <img className="sylveonCutImage" alt="sylveon_01" src="img/sylveonCut.png" style={{resizeMode: "contain", width: "500px", height: "auto"}}/>
                {/* 이미지 크기를 자동으로 조절하는 방법 찾기. */}
            </div>
            <h1>친구가 고민이 있을 때, 내가 하는 행동은?</h1>
            <Link to={"/Result"}>
                <button className='btnQ' onClick={plus1}>말 없이 친구의 이야기를 들어준다.</button>
                <button className='btnQ' onClick={plus2}>현재 상황을 해결할 방법을 제시한다.</button>
                <button className='btnQ' onClick={plus3}>그간 힘들었을 친구를 위로해준다.</button>
            </Link>
        </QuestionTitle>
    )
}

export default Question_2;