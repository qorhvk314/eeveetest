import React from 'react';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Question from './Question';
import Question_1 from './Question_1';
import Question_2 from './Question_2';
import Question_3 from './Question_3';
import Question_4 from './Question_4';
import Question_5 from './Question_5';
import Question_6 from './Question_6';
import Question_7 from './Question_7';
import Question_8 from './Question_8';
import Result from './Result';


const Routess = () => {
    return (
            <Routes>
                <Route path="/" element={<Question />} />
                <Route path="/1" element={<Question_1 />} />
                <Route path="/2" element={<Question_2 />} />
                <Route path="/3" element={<Question_3 />} />
                <Route path="/4" element={<Question_4 />} />
                <Route path="/5" element={<Question_5 />} />
                <Route path="/6" element={<Question_6 />} />
                <Route path="/7" element={<Question_7 />} />
                <Route path="/8" element={<Question_8 />} />
                <Route path="/Result" element={<Result />} />
            </Routes>
    );
  };


export default Routess;