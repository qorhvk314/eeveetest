import React  from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { increaseValues } from './actions';
import { useSelector } from 'react-redux';

const QuestionTitle = styled.div`

    text-align: center;
    position: relative;
    left: 50%;
    transform: translateX(-50%);

    .btnQ {
        top: 50%;
        left: 50%;
        width: 250px;
        margin-top: -86px;
        margin-left: -89px;
        text-align: center;
        -webkit-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        -moz-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        -ms-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        -o-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        display: block;
        margin: 20px auto;
        text-decoration: none;
        border-radius: 10px;
        padding: 10px 20px;
        color: rgba(30, 22, 54, 0.6);
	    box-shadow: rgba(30, 22, 54, 0.4) 0 0px 0px 2px inset;
    }

        .btnQ:hover {
        color: rgba(255, 255, 255, 0.85);
        box-shadow: rgba(30, 22, 54, 0.7) 0 0px 0px 40px inset;
    }

    h1{
        text-align: center;
        font-size: 20px;
        margin-top: 40px;
        margin-bottom: 30px;
    }
    
    `;

function Question_1 () {

    const dispatch = useDispatch();
    const values = useSelector(state => state.values);


    const plus1 = () => {
        dispatch(increaseValues({flareon: values.flareon + 2, leafeon: values.leafeon + 2, sylveon: values.sylveon + 2}));
    }
    
    const plus2 = () => {
        dispatch(increaseValues({vaporeon: values.vaporeon + 2, glaceon: values.glaceon + 2}));
    }
    
    const plus3 = () => {
        dispatch(increaseValues({jolteon: values.jolteon + 2, espeon: values.espeon + 1, umbreon: values.umbreon + 2}));
    }

    console.log(values)

    return (
        <QuestionTitle>
            <div className="Img1">
                <img className="vaporeonCutImage" alt="vaporeon_01" src="img/vaporeonCut.png" style={{resizeMode: "contain", width: "500px", height: "auto"}}/>
                {/* 이미지 크기를 자동으로 조절하는 방법 찾기. */}
            </div>
            <h1>혼자서 여행을 떠나기로 했다. 내가 더 선호하는 장소는?</h1>
            <Link to={"/2"}>
                <button className='btnQ' 
                onClick = {plus1}>따뜻한 햇살이 좋은 산</button>
            </Link>
            <Link to={"/2"}>
                <button className='btnQ'
                onClick = {plus2}>시원한 바다가 보이는 섬</button>
            </Link>
            <Link to={"/2"}>
                <button className='btnQ'
                onClick={plus3}>볼거리가 많아! 도심</button>
            </Link>
        </QuestionTitle>
    )
}


export default Question_1;