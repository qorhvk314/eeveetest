import React  from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { resetValues } from '../app/Reducers';

const QuestionTitle = styled.div`

    text-align: center;
    position: relative;
    left: 50%;
    transform: translateX(-50%);

    .btnQ {
        top: 50%;
        left: 50%;
        width: 250px;
        margin-top: -86px;
        margin-left: -89px;
        text-align: center;
        -webkit-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        -moz-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        -ms-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        -o-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        display: block;
        margin: 20px auto;
        text-decoration: none;
        border-radius: 10px;
        padding: 10px 20px;
        color: rgba(30, 22, 54, 0.6);
	    box-shadow: rgba(30, 22, 54, 0.4) 0 0px 0px 2px inset;
    }

        .btnQ:hover {
        color: rgba(255, 255, 255, 0.85);
        box-shadow: rgba(30, 22, 54, 0.7) 0 0px 0px 40px inset;
    }

    h1{
        text-align: center;
        font-size: 20px;
        margin-top: 40px;
        margin-bottom: 30px;
    }
    
    `;

function Sylveon () {

    const dispatch = useDispatch();

    const handleResetClick = () => {
        dispatch(resetValues());
  };

    return (
        <QuestionTitle>
            <div className="Img1">
                <img className="sylveonImage" alt="sylveon_02" src="img/sylveon.png" style={{resizeMode: "contain", width: "300px", height: "auto"}}/>                
            </div>
            <h1>당신과 잘 맞는 이브이 진화체는 님피아!</h1>
                <img className="sylveonPixelImage" alt="sylveon_03" src="img/sylveonPixel.png" style={{resizeMode: "contain", width: "100px"}}/>
            <Link to={"/"}>
                <button className='btnQ' onClick={handleResetClick}>다시하기</button>
            </Link>
        </QuestionTitle>
    )
};

export default Sylveon;