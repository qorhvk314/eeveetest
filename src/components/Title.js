import React from 'react';
import styled from 'styled-components';

const Title = styled.div`

    font-size: 30px;
    text-align: center;
    margin-top: 30px;
    /* margin-bottom: 30px; */
`;

function MainTitle({ children }) {
    return <Title>{children}</Title>;
  }
  
export default Title;