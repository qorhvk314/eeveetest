import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const QuestionTitle = styled.div`

    text-align: center;
    position: relative;
    left: 50%;
    transform: translateX(-50%);

    h1 {
        font-size: 20px;
        margin-top: 50px;
    }

    .btnQ {
        width: 130px;
        height: 40px;
        line-height: 42px;
        padding: 0;
        border: none;
        background: rgb(255,27,0);
        background: linear-gradient(0deg, rgba(255,27,0,1) 0%, rgba(251,75,2,1) 100%);
        cursor: pointer;
        transition: all 0.3s ease;
        position: relative;
        display: inline-block;
        box-shadow:inset 2px 2px 2px 0px rgba(255,255,255,.5),
        7px 7px 20px 0px rgba(0,0,0,.1),
        4px 4px 5px 0px rgba(0,0,0,.1);
        /* outline: none; */
    }
   
    .btnQ:hover {
        color: #f0094a;
        background: transparent;
        box-shadow:none;
    }
    .btnQ:before,
    .btnQ:after{
        content:'';
        position:absolute;
        top:0;
        right:0;
        height:2px;
        width:0;
        background: #f0094a;
        box-shadow:
        -1px -1px 5px 0px #fff,
        7px 7px 20px 0px #0003,
        4px 4px 5px 0px #0002;
        transition:400ms ease all;
    }
    .btnQ:after{
        right:inherit;
        top:inherit;
        left:0;
        bottom:0;
    }
    .btn-5:hover:before,
    .btn-5:hover:after{
        width:100%;
        transition:800ms ease all;
    }
`;

const MainImgSt = styled.div`

    width: 500px;
    height: 281px;
    object-fit: cover;

    position: relative;
    left: 50%;
    transform: translateX(-50%);

    padding-bottom: 24px;
    border-bottom: 1px solid #e9ecef;

    /* border: 1px solid red; */

`;


function Question () {
    return (
        <QuestionTitle>
            <div className="Img1">
                <img className="eeveeCutImage" alt="eevee_01" src="img/eeveeCut.png" style={{resizeMode: "contain", width: "500px", height: "auto"}}/>
                {/* 이미지 크기를 자동으로 조절하는 방법 찾기. */}
            </div>
            <hr />
            <h1>나에게 잘 맞는 이브이 진화체 알아보기</h1>
            <Link to={"/1"}>
                <button className='btnQ'>START</button>
            </Link>
        </QuestionTitle>
    )
}

export default Question;