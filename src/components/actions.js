export const increaseValues = (values) => {
  return {
    type: 'INCREASE_VALUES',
    payload: values,
  };
};
