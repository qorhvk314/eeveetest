import { useSelector, useDispatch } from 'react-redux';
import Vaporeon from './Vaporeon';
import Jolteon from './Jolteon';
import Flareon from './Flareon';
import Espeon from './Espeon';
import Umbreon from './Umbreon';
import Leafeon from './Leafeon';
import Glaceon from './Glaceon';
import Sylveon from './Sylveon';

function Result() {
  const { vaporeon, jolteon, flareon, espeon, umbreon, leafeon, glaceon, sylveon } = useSelector(state => state.values);

  let highestValue = 0;
  let highestPokemon = '';

  // Store에서 가장 높은 값을 찾아냅니다.
  for (const [key, value] of Object.entries({ vaporeon, jolteon, flareon, espeon, umbreon, leafeon, glaceon, sylveon })) {
    if (value > highestValue) {
      highestValue = value;
      highestPokemon = key;
    }
  }

  // 가장 높은 값에 해당하는 결과를 출력합니다.
  switch (highestPokemon) {
    case 'vaporeon':
      return <Vaporeon />;
    case 'jolteon':
      return <Jolteon />;
    case 'flareon':
      return <Flareon />;
    case 'espeon':
      return <Espeon />;
    case 'umbreon':
      return <Umbreon />;
    case 'leafeon':
      return <Leafeon />;
    case 'glaceon':
      return <Glaceon />;
     case 'sylveon':
      return <Sylveon />;
     
    default:
      return null;
  }
}


export default Result;