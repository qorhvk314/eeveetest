import React from 'react';
import styled from 'styled-components';

const EeveeDeco = styled.div`
    height: 50px;
    
    position: relative;
    transform: translateX(-50%);
    left: 50%;
    flex-direction: row;
    display: flex;
    align-items: center;

    margin-bottom: 40px;
    /* border: 1px solid red; */

`;

function Deco() {
    return (
      <EeveeDeco>
          <div className="Img2">
              <div className="eevee">
                  <img className="eeveePixelImage" alt="eevee_02" src="Img/eeveePixel.png" style={{resizeMode: "contain", width: "58px", height: "auto"}}/>
                  <img className="vaporeonPixelImage" alt="vaporeon_02" src="Img/vaporeonPixel.png" style={{resizeMode: "contain", width: "58px", height: "auto"}}/>
                  <img className="jolteonPixelImage" alt="jolteon_02" src="Img/jolteonPixel.png" style={{resizeMode: "contain", width: "58px", height: "auto"}}/>
                  <img className="flareonPixelImage" alt="flareon_02" src="Img/flareonPixel.png" style={{resizeMode: "contain", width: "58px", height: "auto"}}/>
                  <img className="espeonPixelImage" alt="espeon_02" src="Img/espeonPixel.png" style={{resizeMode: "contain", width: "50px", height: "auto", marginRight: "4px"}}/>
                  <img className="umbreonPixelImage" alt="umbreon_02" src="Img/umbreonPixel.png" style={{resizeMode: "contain", width: "50px", height: "auto", marginRight: "4px"}}/>
                  <img className="glaceonPixelImage" alt="glaceon_02" src="Img/glaceonPixel.png" style={{resizeMode: "contain", width: "50px", height: "auto", marginRight: "4px"}}/>
                  <img className="leafeonPixelImage" alt="leafeon_02" src="Img/leafeonPixel.png" style={{resizeMode: "contain", width: "50px", height: "auto", marginRight: "4px"}}/>
                  <img className="sylveonPixelImage" alt="sylveon_02" src="Img/sylveonPixel.png" style={{resizeMode: "contain", width: "50px", height: "auto", marginRight: "4px"}}/>
              </div>

          </div>
      </EeveeDeco>
    );
  }
  
  export default Deco;