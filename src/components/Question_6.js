import React  from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { increaseValues } from './actions';
import { useSelector } from 'react-redux';

const QuestionTitle = styled.div`

    text-align: center;
    position: relative;
    left: 50%;
    transform: translateX(-50%);

    .btnQ {
        top: 50%;
        left: 50%;
        width: 250px;
        margin-top: -86px;
        margin-left: -89px;
        text-align: center;
        -webkit-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        -moz-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        -ms-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        -o-transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        transition: all 200ms cubic-bezier(0.390, 0.500, 0.150, 1.360);
        display: block;
        margin: 20px auto;
        text-decoration: none;
        border-radius: 10px;
        padding: 10px 20px;
        color: rgba(30, 22, 54, 0.6);
	    box-shadow: rgba(30, 22, 54, 0.4) 0 0px 0px 2px inset;
    }

        .btnQ:hover {
        color: rgba(255, 255, 255, 0.85);
        box-shadow: rgba(30, 22, 54, 0.7) 0 0px 0px 40px inset;
    }

    h1{
        text-align: center;
        font-size: 20px;
        margin-top: 40px;
        margin-bottom: 30px;
    }

`;

function Question_2 () {

    const dispatch = useDispatch();
    const values = useSelector(state => state.values);

    const plus1 = () => {
        dispatch(increaseValues({vaporeon: values.vaporeon + 1, sylveon: values.espeon + 1, leafeon: values.leafeon + 1}));
    }
    
    const plus2 = () => {
        dispatch(increaseValues({glaceon: values.glaceon + 1, umbreon: values.umbreon + 1}));
    }
    
    const plus3 = () => {
        dispatch(increaseValues({espeon: values.sylveon + 1, flareon: values.flareon + 1, jolteon: values.jolteon + 1}));
    }

    console.log(values)

    return (
        <QuestionTitle>
            <div className="Img1">
                <img className="glaceonCutImage" alt="glaceon_01" src="img/glaceonCut.png" style={{resizeMode: "contain", width: "500px", height: "auto"}}/>
                {/* 이미지 크기를 자동으로 조절하는 방법 찾기. */}
            </div>
            <h1>애인이 나 몰래 바람을 피웠다. 이 사실을 알았을 때?</h1>
            <Link to={"/7"}>
                <button className='btnQ' onClick={plus1}>잠깐의 실수겠거니. 용서한다.</button>
                <button className='btnQ' onClick={plus2}>다시는 보고싶지 않다. 대화를 않는다.</button>
                <button className='btnQ' onClick={plus3}>어떻게 나한테! 길길이 화를 낸다.</button>
            </Link>
        </QuestionTitle>
    )
}

export default Question_2;