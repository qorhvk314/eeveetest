import { configureStore } from '@reduxjs/toolkit';
import { values } from './Reducers';

const initialState = {
  vaporeon: 0,
  jolteon: 0,
  flareon: 0,
  espeon: 0,
  umbreon: 0,
  leafeon: 0,
  glaceon: 0,
  sylveon: 0
};

const reducers = {
  values: values // values 리듀서를 'values' 상태 객체로 할당
}

export default configureStore({
  reducer: reducers, // 'reducer' 옵션으로 수정된 reducers 객체 전달
  preloadedState: initialState
});
