import { combineReducers } from 'redux';
import { INCREASE_VALUES } from '../components/actions';

const initialState = {
  vaporeon: 0,
  jolteon: 0,
  flareon: 0,
  espeon: 0,
  umbreon: 0,
  leafeon: 0,
  glaceon: 0,
  sylveon: 0
  };

  
  
  const Reducers = (state = initialState, action) => {
    switch (action.type) {
      case 'INCREASE_VALUES':
        return {
          ...state,
          flareon: action.payload.flareon !== undefined ? action.payload.flareon : state.flareon,
          leafeon: action.payload.leafeon !== undefined ? action.payload.leafeon : state.leafeon,
          sylveon: action.payload.sylveon !== undefined ? action.payload.sylveon : state.sylveon,
          vaporeon: action.payload.vaporeon !== undefined ? action.payload.vaporeon : state.vaporeon,
          glaceon: action.payload.glaceon !== undefined ? action.payload.glaceon : state.glaceon,
          jolteon: action.payload.jolteon !== undefined ? action.payload.jolteon : state.jolteon,
          espeon: action.payload.espeon !== undefined ? action.payload.espeon : state.espeon,
          umbreon: action.payload.umbreon !== undefined ? action.payload.umbreon : state.umbreon
        };
      default:
        return state;
    }
  };

  // console.log('Current state:', Reducers(undefined, { type: 'INIT' }));
  
  export const resetValues = () => {
    return {
      type: 'RESET_VALUES',
      payload: {
        vaporeon: 0,
        jolteon: 0,
        flareon: 0,
        espeon: 0,
        umbreon: 0,
        leafeon: 0,
        glaceon: 0,
        sylveon: 0
      }
    }
  };
  
  export function values(state = initialState, action) {
    switch (action.type) {
      case 'INCREASE_VALUES':
        return { ...state, ...action.payload };
      case 'RESET_VALUES':
        return initialState;
      default:
        return state;
    }
  }