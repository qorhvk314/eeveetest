import React from 'react';
import { createGlobalStyle } from 'styled-components';
import Template from './components/Template';
import Title from './components/Title';
import Deco from './components/Deco';
import Routess from './components/Routess';

const GlobalStyle = createGlobalStyle`
  body {
    background: #e9ecef;
  }
`;

function App() {
  return (
    <>
      <GlobalStyle />
      <Template>
        <Title> 나에게 잘 맞는 이브이 진화체는? </Title>
        <Deco />
        <Routess />
      </Template>
    </>
  );
}

export default App;